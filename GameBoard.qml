import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.1
import Qt.labs.folderlistmodel 1.0


Rectangle {
    id: gameBoard
    width: parent.width
    height: parent.height
    color: "#A1DD64"

    property real effectiveOpacity: 1.0
    property real ratio: width / 320 < height / 440 ? width / 320 : height / 440
    property int smallSize: 45 * ratio
    property int bigSize: 2 * smallSize
    property int elementSpacing: 0.14 * smallSize

    property int cardSize: gameBoard.height/6.5
    property int cardNumberHorizontal: 8
    property int cardNumberVertical: 5
    property int cardsHorizontalSize: cardSize*cardNumberHorizontal + (cardNumberHorizontal-1)*cardSize/10
    property int cardsVerticalSize: cardSize*cardNumberVertical + (cardNumberVertical-1)*cardSize/10
    property var randomArray: setRandomArray()

    property int firstCard: -1
    property int secondCard: -1
    property int indexFirstCard: -1
    property int indexSecondCard: -1

    property bool flipBlocked: false

    property int disappearanceTime: 2000
    property int memorizeTime: 2000

    // Can be removed, isn't needde atm
    FileDialog {
        id: fileDialog
        title: "Choose a folder with some images"
        selectFolder: true
        onAccepted: folderModel.folder = fileUrl + "/"
    }

    Timer {
        id: turnTimer
        interval: memorizeTime
        running: false
        repeat: false
        //onTriggered: effectiveOpacity = (effectiveOpacity == 1.0 ? 0.0 : 1.0);
        onTriggered: {
            //console.log("Timer:" + indexFirstCard + " " + indexSecondCard)
            cards.itemAt(indexFirstCard).flipped = false
            cards.itemAt(indexSecondCard).flipped = false
            indexFirstCard = -1
            indexSecondCard = -1
            flipBlocked = false
            playerTimeRectangle.width = 200
        }
    }

    Rectangle {
        id: playerTimeRectangle
        x: gameBoard.width/2-100; y: 20
        height: 10; width: 200
        color: "#F37F41"

        Behavior on width {
            NumberAnimation {
                duration: memorizeTime
                from: 200
            }
        }
    }

    Grid {
        id: cardGrid
        x: (parent.width-cardsHorizontalSize)/2
        y: (parent.height-cardsVerticalSize)/2

        columns: cardNumberHorizontal
        rows:cardNumberVertical
        spacing: cardSize/10

        populate: Transition {
            NumberAnimation { properties: "x,y"; from: 200; duration: 100; easing.type: Easing.OutBounce }
        }
        add: Transition {
            NumberAnimation { properties: "x,y"; easing.type: Easing.OutBounce }
        }
        move: Transition {
            NumberAnimation { properties: "x,y"; easing.type: Easing.OutBounce }
        }

        property int fileIndex: 0;
        Repeater {
            id: cards

            model: FolderListModel {
                id: folderModel
                folder: "file:///home/fmario/devel/Paarlini/img/"
                objectName: "folderModel"
                showDirs: false
                nameFilters: ["*.png", "*.jpg", "*.gif"]
            }

            Flipable {
                id: flipable
                objectName: "flipable" + index
                width: cardSize
                height: cardSize

                property bool flipped: false

                front: Rectangle {
                    radius: cardSize/10
                    anchors.fill: parent
                    color: "#49801D"
                }
                back: Rectangle {
                    radius: 10
                    anchors.fill: parent
                    color: "white"

                    Image {
                        rotation: Math.floor(Math.random()*100)%4*90
                        width: gameBoard.height/6
                        height: gameBoard.height/6
                        id: image
                        anchors.centerIn: parent
                        fillMode: Image.PreserveAspectFit
                        source: folderModel.folder + folderModel.get(randomArray[index]-1, "fileName")
                        antialiasing: true
                    }
                    Text {
                        //text: randomArray[index]
                    }
                }

                Behavior on opacity {

                    NumberAnimation {
                        duration: disappearanceTime
                    }
                }

                transform: Rotation {
                    id: rotation
                    origin.x: flipable.width/2
                    origin.y: flipable.height/2
                    axis.x: 0; axis.y: 1; axis.z: 0     // set axis.y to 1 to rotate around y-axis
                    angle: 0    // the default angle
                }

                states: State {
                    name: "back"
                    PropertyChanges { target: rotation; angle: 180 }
                    when: flipable.flipped
                }

                transitions: Transition {
                    NumberAnimation { target: rotation; property: "angle"; duration: 200 }
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if (!flipBlocked && indexFirstCard != index) {
                            flipable.flipped = !flipable.flipped

                            // Nice possible feature: cards disappear with every turn
                            flipable.opacity -= 0.1

                            // The first card was turned...
                            if (firstCard == -1) {
                                firstCard = randomArray[index]
                                indexFirstCard = index
                                //console.log("First if: " + firstCard)
                            // The second card was turned
                            } else if (secondCard == -1) {
                                secondCard = randomArray[index]
                                indexSecondCard = index
                                //console.log("Second if:" + secondCard + " - " + firstCard)

                                // We found a matching pair ;-)
                                if (firstCard-secondCard == 20 || firstCard-secondCard == -20) {
                                    //console.log("Yes, pair found.")
                                    cards.itemAt(indexFirstCard).opacity = 0.0
                                    cards.itemAt(index).opacity = 0.0
                                // The pair didn't match ;-(
                                } else {
                                    flipBlocked = true
                                    playerTimeRectangle.width = 0
                                    turnTimer.start()
                                    //console.log("Not equal")
                                }
                                firstCard =  -1
                                //indexFirstCard = -1
                                //indexSecondCard = -1
                                secondCard = -1
                            }
                        }
                    }
                }

                //Item {
                //            Component.onCompleted: console.log('Component ' + index + ' completed!')
                //}
            }
        }
    }
    function setRandomArray() {
        var arr = []
        while(arr.length < 40){
          var randomnumber=Math.ceil(Math.random()*100%40)
          var found=false;
          for(var i=0;i<arr.length;i++){
                if(arr[i]==randomnumber){found=true;break}
          }
          if(!found)arr[arr.length]=randomnumber;
        }
        return arr
    }

    //Component.onCompleted: fileDialog.open()
}

