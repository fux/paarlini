import QtQuick 2.4
import QtQuick.Window 2.2

Window {
    id: root
    visible: true
    //visibility: QWindow::FullScreen
    width: Screen.width
    height: Screen.height
    color: "darkgreen"

    GameBoard {
        id: gameBoard
    }

    Rectangle {
        height: parent.height/20
        width: parent.width/10
        color: "#F37F41"

        Text {
            anchors.centerIn: parent
            text: qsTr("Back to Menu")
            font.pointSize: 16
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                startScreen.x = 0
                startScreen.y = 0
            }
        }
    }

    StartScreen {
        id: startScreen
    }







   // MainForm {
   //     anchors.fill: parent
   //     mouseArea.onClicked: {
   //         Qt.quit();
   //     }
   //
   // }
}
