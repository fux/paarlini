import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Rectangle {
    id: startScreen
    width: parent.width
    height: parent.height
    color: "#006DB6"

    MouseArea {
        anchors.fill: parent
    }

    Behavior on x {
        NumberAnimation {
            duration: 1000
            easing.type: Easing.InOutQuint
        }
    }

    Behavior on y {
        NumberAnimation {
            duration: 1000
            easing.type: Easing.InOutQuint
        }
    }

    Column {
        anchors.centerIn: parent
        spacing: 10

        Button {
            width: 200
            height: 100
            id: twoPlayersButton
            text: qsTr("Two Players")
            onClicked: startScreen.y = startScreen.height
            style: ButtonStyle {
                label: Text {
                     renderType: Text.NativeRendering
                      verticalAlignment: Text.AlignVCenter
                      horizontalAlignment: Text.AlignHCenter
                      font.family: "Helvetica"
                      font.pointSize: 20
                      color: "black"
                      text: control.text
                 }
            }
        }

        Button {
            width: 200
            height: 100
            id: threePlayersButton
            text: qsTr("Three Players")
            onClicked: startScreen.x = startScreen.width
            style: ButtonStyle {
                label: Text {
                     renderType: Text.NativeRendering
                      verticalAlignment: Text.AlignVCenter
                      horizontalAlignment: Text.AlignHCenter
                      font.family: "Helvetica"
                      font.pointSize: 20
                      color: "black"
                      text: control.text
                 }
            }
        }

        Button {
            width: 200
            height: 100
            id: fourPlayersButton
            text: qsTr("Four Players")
            onClicked: startScreen.x = -startScreen.width
            style: ButtonStyle {
                label: Text {
                     renderType: Text.NativeRendering
                      verticalAlignment: Text.AlignVCenter
                      horizontalAlignment: Text.AlignHCenter
                      font.family: "Helvetica"
                      font.pointSize: 20
                      color: "black"
                      text: control.text
                 }
            }
        }

        Button {
            width: 200
            height: 100
            id: quitButton
            text: qsTr("Quit Game")
            onClicked: Qt.quit()
            style: ButtonStyle {
                label: Text {
                     renderType: Text.NativeRendering
                      verticalAlignment: Text.AlignVCenter
                      horizontalAlignment: Text.AlignHCenter
                      font.family: "Helvetica"
                      font.pointSize: 20
                      color: "black"
                      text: control.text
                 }
            }
        }
    }
}

